import argparse
from io import StringIO

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser

def read_file_bytes(path: str):
    with open(path, 'rb') as f:
        return f.read()

def output_bytes_hex(data: bytes, cols: int = 16):
    output = StringIO()
    for i in range((len(data) // cols) + 1):
        for j in range(cols):
            idx = (i * cols) + j
            if len(data) <= idx:
                break
            if j != 0:
                output.write(' ')
            output.write('0x{:02X},'.format(data[idx]))
        output.write('\n')
    return output.getvalue()

def main():
    args = get_parser().parse_args()
    data = read_file_bytes(args.file)
    hex = output_bytes_hex(data)
    print(hex)

if __name__ == '__main__':
    main()
