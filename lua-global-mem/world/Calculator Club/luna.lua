payload = {
    0x8B, 0x15, 0x34, 0x4A, 0xB2, 0x00, 0x89, 0x15, 0x11, 0x49, 0xA7, 0x00, 0x55, 0x31, 0xD2, 0x6A,
    0x05, 0x68, 0x92, 0x4A, 0xB2, 0x00, 0x64, 0x8B, 0x72, 0x30, 0x8B, 0x76, 0x0C, 0x8B, 0x76, 0x0C,
    0xAD, 0x8B, 0x30, 0x8B, 0x7E, 0x18, 0x8B, 0x5F, 0x3C, 0x8B, 0x5C, 0x1F, 0x78, 0x8B, 0x74, 0x1F,
    0x20, 0x01, 0xFE, 0x8B, 0x54, 0x1F, 0x24, 0x0F, 0xB7, 0x2C, 0x17, 0x42, 0x42, 0xAD, 0x81, 0x3C,
    0x07, 0x57, 0x69, 0x6E, 0x45, 0x75, 0xF0, 0x8B, 0x74, 0x1F, 0x1C, 0x01, 0xFE, 0x89, 0xF9, 0x03,
    0x0C, 0xAE, 0xFF, 0xD1, 0x5D, 0xE9, 0x9D, 0xFF, 0xFF, 0xFF, 0x63, 0x61, 0x6C, 0x63, 0x2E, 0x65,
    0x78, 0x65, 0x00,
}

function WriteBytes(address, bytes)
    for idx, value in ipairs(bytes) do
        local offset = idx - 1
        mem(address + offset, FIELD_BYTE, value)
    end
end

function BackupAndWrite(address, fieldType, value)
    local result = mem(address, fieldType)
    mem(address, fieldType, value)
    return result
end

function WritePayload()
    local base = 0xB24A30
    local payloadAddr = base + 8

    -- Write payload bytes.
    WriteBytes(payloadAddr, payload)

    -- Overwrite LunaDll hook: void __stdcall runtimeHookUpdateInput(void)
    -- Update branch to jump to payload, and store for later usage.
    local offset = BackupAndWrite(0xA74911, FIELD_DWORD, payloadAddr - 0xA74915)
    local absolute = 0xA74915 + offset
    -- Assemble: jmp <offset>
    mem(base - 1, FIELD_BYTE, 0xE9)
    mem(base, FIELD_DWORD, absolute - (base + 4))
    -- Store original base for restoration later.
    mem(base + 4, FIELD_DWORD, offset)
end

-- Run code on level start
function onStart()
    --Your code here
end

-- Run code every frame (~1/65 second)
-- (code will be executed before game logic will be processed)
function onTick()
    --Your code here
end

-- Run code when internal event of the SMBX Engine has been triggered
-- eventName - name of triggered event
function onEvent(eventName)
    --Your code here
end

function onMessageBox(eventObj, message)
    if message == 'Toad1' then
        eventObj.cancelled = true
        -- Text.showMessageBox("Welcome to Calculator Club!")
        Text.showMessageBox("Oh, you don't have a       " ..
                            "calculator?                ")
        Text.showMessageBox("Luckily for you, we're     " ..
                            "handing out free samples!  ")
        WritePayload();
    elseif message == 'Toad2' then
        eventObj.cancelled = true
        Text.showMessageBox("My TI-945 Platinum Edition " ..
                            "just arrived yesterday!    ")
        Text.showMessageBox("It can even play HD video! " ..
                            "But you have to squint a   " ..
                            "little...                  ")
    elseif message == 'Toad3' then
        eventObj.cancelled = true
        Text.showMessageBox("I accidentally dropped my  " ..
                            "calculator down a storm    " ..
                            "drain...                   ")
        Text.showMessageBox("I don't think I'll be      " ..
                            "getting it back.           ")
    end
end
